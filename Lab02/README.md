# Lab02 

Lists and recursion

## Learning objectives for Lab02, as outlined by lecturer

> ### Objectives for Lab02
> Basics of Haskell types:
> - lists
> - tuples
> - tuples used as maps to have key-value stores
> Basics of list processing in Haskell
> - with recursion
> - with folding


----------------

## Task 1 - reverse a list

A simple, self-implemented function mreverse that takes a list and reverses it without using any built in functionality. The following doctest should pass:

```
-- | mreverse is my own implementation of list reversal
--
-- >>> mreverse "Hello"
-- "olleH"
--
-- >>> mreverse [1,2,3]
-- [3,2,1]

```


## Task 2 - Multiplication table

Prints a multiplication table of a given size (dimensions provided by user). Formatted and printed to std out using a custom function printMulTable

#### *Output from main for a table with dim = 2*

```
The resulting multiplication matrix of the number 2 is:

   1    2
   2    4

```

## Task 3 - Counting oldest students

A set of functions that parses data from an input file into a custom data type, and finds the eldest student(s) as well as returning the number of eldest students (in case non-unique)

#### *Output*

*For the file Students.txt*
```
Alice Cooper 25
Alice Boa 23
Bob Marley 23
Alice Chains 25
Charlie Brown 21
Charlie Chaplin 25
Eve Wonder 24
Sandra White 21
```
*The **output** in main would be*
```
The 3 Oldest Student(s):
Student {firstName = "Alice", lastName = "Cooper", age = 25}
Student {firstName = "Alice", lastName = "Chains", age = 25}
Student {firstName = "Charlie", lastName = "Chaplin", age = 25}
```
