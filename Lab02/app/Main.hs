module Main (main) where

import Lib
-- added import for formatting matrices:
import Text.Printf


main :: IO ()
main = do

-- Task 1:
  -- setting up the lists
  let mList = [1,2,3,4]
  let wrd = "Hello"

  -- creating output and using mreverse on both lists
  putStrLn $ "\n\tTask 1:\n\nThe reverse of the list " ++ show mList ++ " is:\t" ++ show (mreverse mList)
  putStrLn $ "\nThe reverse of the word " ++ show wrd ++ " is:\t" ++ show (mreverse wrd)

-- Task 2:
  -- writing a simple user input and matrix output sequence
  putStrLn "\n\tTask 2:\n\nPlease enter the number to multiply (the dimension of the matrix):"
  input <- getLine
  -- parsing it into an Int, using read
  let number = read input :: Int
  -- creating the matrix using the mulTable function
  let matrix = mulTable number
  -- printing the resulting matrix to the terminal using printMulTable
  putStrLn $ "\nThe resulting multiplication matrix of the number " ++ show number ++ " is:\n"
  printMulTable matrix

-- Task 3:
  -- retrieving the contents of the file with students (using the example file from the gitlab course repo)
  -- setting up the filepath in a variable, parsing the lines of the file into a string 'content' using readFile
  let fPath = "src/Students.txt"
  content <- readFile fPath

  -- mapping the files 'content' string to create Student data types using pattern matching, adding these to the map
  let students = map toStudent (lines content)
  -- finding the oldest student(s) and counts the elements in the resulting list/map
  let (count, oldest) = oldestStuds students

  -- prints the students with maxAge to the terminal, and the number of students with maxAge
  putStrLn $ "\nThe " ++ show count ++ " Oldest Student(s): "
  mapM_ print oldest
