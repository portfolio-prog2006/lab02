module Lib
    ( someFunc,
    mreverse,
    mulTable,
    printMulTable,
    toStudent,
    oldestStuds
    ) where

  -- dependency th-printf added in package.yaml
import Text.Printf


  -- | Default test-function
someFunc :: IO ()
someFunc = putStrLn "someFunc"


-- Task 1: List reversal

-- | List reversal function without using the built-in one ♥ implemented using recursion, having the list call
-- itself with the tail of the list 'xs' and append the head of the list 'x'
--
-- >>> mreverse "Hello"
-- "olleH"
--
-- >>> mreverse [1,2,3]
-- [3,2,1]
mreverse :: [a] -> [a]
mreverse [] = []
mreverse (x:xs) = mreverse xs ++ [x]



-- Task 2: multiplication matrix

-- | Implementation of a multiplication table function of a given size n using nesting and guards. It takes
-- an int n and returns a nested list (a matrix) of dimension n
--
mulTable :: Int -> [[Int]]
mulTable n = [[i * j | j <- [1..n]] | i <- [1..n]]

-- | Task 2 : Padding function
-- A print function for the mulTable above, with (some) formatting. Used monads to both
-- print and format the individual elements in the list before moving on to the next element.
-- The [[Int]] denotes that it's a matrix (a list of lists,
-- which translates to a row where each of the elements contain a separate list).
-- The return type is IO (to print to terminal), the formatRow is a local function defined within printMulTable, and the '.' is the function
-- composition operator, which allows me to use the result of one function and pass that to the other within the monad mapM_.
-- The unwords std library function concatenates strings (which is a list) into a single one with (here) 4 spaces for each number.
--
printMulTable :: [[Int]] -> IO ()
printMulTable mTable = mapM_ (putStrLn . formatRow) mTable
  where
    formatRow row =  unwords $ map (printf "%4d") row



-- Task 3: Counting ancient students

-- | First, creating the data type 'Student' with three parameters/list-elements (firstname, lastname, age) to be used
-- when parsing lines from the file into these student data types (using pattern matching), and then finding and counting the oldest one(s)
-- the 'deriving (Show and Eq)' is used to derive show and eq instances, so I can display the resulting string and use comparison on the data type
data Student = Student { firstName :: String, lastName :: String, age :: Int } deriving (Show, Eq)

-- | Secondly, we need a helper function to parse lines from the file into the Student data type defined above
-- this uses pattern matching and destructuring, using 'words' to turn the input line into separate words stored one-by-one ('words fileInput')
-- in a list, which is the opposite action to 'unwords' used above.
-- Then, this list is matched against the Student structure of [firstName, lastName, age], and a Student has been created from the input in case of match
toStudent :: String -> Student
toStudent fileInput = case words fileInput of
  [firstName, lastName, ageString] -> Student firstName lastName (read ageString)
  _ -> error "Invalid format"

-- | Task 3, cont.: Finally, we need a function that finds and counts the oldest of the students. It should take a
-- [Student], which is a list of Student data types, as input and return a typle of an Int (the counter, the number of students in [Student]) and a list of the Student(s) with age == maxAge
oldestStuds :: [Student] -> (Int, [Student])
oldestStuds students = let
  maxAge = maximum $ map age students
  oldest = filter (\s -> age s == maxAge) students
  in (length oldest, oldest)